#!/usr/bin/env python2
from TransactionManager.TransactionManager import TransactionManager
from Scheduler.Scheduler import Scheduler
from Scheduler.OCCScheduler import OCCScheduler
from Scheduler.TwoPLScheduler import TwoPLScheduler
from Logger.Logger import Logger
from DataManager.DataManager import DataManager
import sys
import argparse

def main(argv):
    # The following lines parse the command line arguments
    argument_parser = argparse.ArgumentParser("myPTA: my Pitt Transaction Agent ")
    argument_parser.add_argument('-d', '--directory', required=True,
                                 help='Path to the directory containing the script files to be processed by the system.')
    # argument_parser.add_argument('-b', '--bufferSize', help='The database buffer size in terms of number of slotted pages in the database buffer', required=True, type=int)
    argument_parser.add_argument('-s', '--seed', type=int, default=50,
                                 help='The seed of the random number generator to be used by the system')
    argument_parser.add_argument('--executionType', type=int, default=1,
                                 help='The execution type; 1 for round robin, 0 for random')
    args = argument_parser.parse_args()

    # Create an object of Transaction Manager
    # Run transaction Manager in default mode with Round Robin scheduler
    logger = Logger()
    datamanager = DataManager(logger)
    occ = OCCScheduler(datamanager, logger)
    twopl = TwoPLScheduler(datamanager, logger)
    #occ=twopl
    TM = TransactionManager(occ, twopl,args.directory, args.seed,
                            executionType=args.executionType, restartThreshold=10)
    TM.execute()

    datamanager.write_all_pages()
    logger.writelog()

if __name__ == "__main__":
    main(sys.argv[1:])