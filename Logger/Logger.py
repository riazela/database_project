# By Kadie Clancy
import json
from DataManager.DataManager import Record
from TransactionManager.TransactionManager import command

class Logger(object):

    def __init__(self):
        self.rollbackLog = []
        with open('log.json', mode='w') as self.f:
            json.dump([], self.f)
        with open('log.json') as self.file:
            self.log = json.load(self.file)
            self.file.close()


    '''
        Scheduler Function: command updates to the log file 
            cmmd: command object
    '''
    def logCommand(self, commd):
        if (commd.commandtype == 'B') or (commd.commandtype == 'C') or (commd.commandtype == 'A'):
            logEntry = {'TID': commd.transactionId,
                        'Command': [commd.commandtype]}
        elif (commd.commandtype == 'R') or (commd.commandtype == 'M'):
            logEntry = {'TID': commd.transactionId,
                        'Command': [commd.commandtype, commd.tableName, commd.recordId]}
        elif commd.commandtype == 'D':
            logEntry = {'TID': commd.transactionId,
                        'Command': [commd.commandtype, commd.tableName]}
        elif commd.commandtype == 'W':
            logEntry = {'TID': commd.transactionId,
                        'Command': [commd.commandtype, commd.tableName, commd.recordId, commd.recordClientName, commd.recordPhone]};
        self.log.append(logEntry)
        #with open('log.json', 'w') as outfile:
            #json.dump(self.log, outfile, indent=4)
            #outfile.close()

    '''
        Scheduler Function: transaction restart update the log file
            transactionNum: Transaction ID
    '''
    def logRestart(self, transactionNum):
        logEntry = {'TID': transactionNum,
                        'Command': 'Restart'}
        self.log.append(logEntry)
        #with open('log.json', 'w') as outfile:
            #json.dump(self.log, outfile, indent=4)
            #outfile.close()

    '''
        Data Manager Function: table updates, deletions, and insertions recorded to log file and records rollback
            operations to separate list
            transactionNum: Transaction ID
            table: table name
            rOld: before image of the record (if = '' insertion)
            rNew: after image of the record (if = '' deletion)
    '''
    def logUpdate(self, transactionNum, table, rOld=None , rNew=None):
        if rOld is not None:
            tmp = Record(rOld.id,rOld.phone,rOld.name)
            rOld = tmp
        if rNew is not None:
            tmp = Record(rNew.id, rNew.phone, rNew.name)
            rNew = tmp
        if rOld == None:
            logEntry = {'TID': transactionNum,
                    'Command': 'Insert',
                    'Table': table,
                    'Record:': [rNew.id, rNew.name, rNew.phone]}
            rollbackEntry = [transactionNum, 'W', table, rOld, rNew]
        elif rNew == None:
            logEntry = {'TID': transactionNum,
                    'Command': 'Delete',
                    'Table': table,
                    'Record:': [rOld.id, rOld.name, rOld.phone]}
            rollbackEntry = [transactionNum, 'D', table, rOld, rNew]
        else:
            logEntry = {'TID': transactionNum,
                        'Command': 'Update',
                        'Table': table,
                        'BeforeImage': [rOld.id, rOld.name, rOld.phone],
                        'AfterImage:': [rNew.id, rNew.name, rNew.phone]}
            rollbackEntry = [transactionNum, 'W', table, rOld, rNew]
        self.log.append(logEntry)
        #with open('log.json', 'w') as outfile:
            #json.dump(self.log, outfile, indent=4)
            #outfile.close()
        self.rollbackLog.append(rollbackEntry)

    '''
         Data Manager Function: update the log file with records being read
                transactionNum: Transaction ID
                table: table where record is being read from
                record: record value being read
    '''
    def logRead(self, transactionNum, table, record):
        logEntry = {'TID': transactionNum,
                    'Command': 'Read',
                    'Table': table,
                    'Record': [record.id, record.name, record.phone]}
        self.log.append(logEntry)
        #with open('log.json', 'w') as outfile:
            #json.dump(self.log, outfile, indent=4)
            #outfile.close()

    '''
         Data Manager Function: update the log file for page swaps
                action: 'swap in' or 'swap out'
                table: table name
                page: page number
                bucket: hash bucket number
    '''
    def logSwap(self, action, table, page, bucket):
        logEntry = {'Action': action,
                    'Table': table,
                    'Page': page,
                    'Bucket': bucket}
        self.log.append(logEntry)
        #with open('log.json', 'w') as outfile:
            #json.dump(self.log, outfile, indent=4)
            #outfile.close()

    '''
         Data Manager Function: update the log file for M Reads
                transactionNum: Transaction ID
                table: table being read from
                records: list of records being read
    '''
    def logMRead(self, transactionNum, table, records):
        list = []
        for x in records:
            list.append([x.id, x.name, x.phone])
        logEntry = {'TID': transactionNum,
                    'Command': 'MRead',
                    'Table': table,
                    'Record': list}
        self.log.append(logEntry)
        #with open('log.json', 'w') as outfile:
            #json.dump(self.log, outfile, indent=4)
            #outfile.close()

    '''
        Log switch from scheduler
        schedulerCurrent: current scheduler 
    '''
    def logSchedulerSwitch(self, schedulerCurrent, newScheduler):
        logEntry = {'CurrentScheduler': schedulerCurrent,
                    'SwitchTo': newScheduler}
        self.log.append(logEntry)
        #with open('log.json', 'w') as outfile:
            #json.dump(self.log, outfile, indent=4)
            #outfile.close()

    '''
        Return list of commands to undo transaction
        transactionNum: transaction ID being aborted
        rollbackEntry = [transactionNum, 'W', table, rOld, rNew]
    '''
    def getRollbackCommands(self, transactionNum):
        rollbacks = []
        #print(self.rollbackLog)
        for x in self.rollbackLog:
            if x[0] == transactionNum:
                if x[1] == 'W':
                    # If update
                    if x[3] != None:
                        cmmdStr = 'W ' + x[2] + ' (' + str(x[3].id) + ', ' + x[3].name + ', ' + x[3].phone+ ')'
                        cmmd = command(cmmdStr, transactionNum)
                        cmmd.processCommand()
                        rollbacks.append(cmmd)
                    else:
                        cmmdStr = 'D ' + x[2] + ' (' + str(x[4].id) + ', ' + x[4].name + ', ' + x[4].phone + ')'
                        cmmd = command(cmmdStr, transactionNum)
                        cmmd.processCommand()
                        rollbacks.append(cmmd)
                elif x[1] == 'D':
                    cmmdStr = 'W ' + x[2] + ' (' + str(x[3].id) + ', ' + x[3].name + ', ' + x[3].phone + ')'
                    cmmd = command(cmmdStr, transactionNum)
                    cmmd.processCommand()
                    rollbacks.append(cmmd)
        return list(reversed(rollbacks))

    def writelog(self):
        with open('log.json', 'w') as outfile:
            json.dump(self.log, outfile, indent=4)
            outfile.close()