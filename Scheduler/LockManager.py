# Lock Manager
#
# Implements the necessary lock hierarchy and supporting classes used by the
# two-phase locking scheduler. Not thread safe.
#
# Copyright (C) 2017 Patrick Gauvin
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
# USA
class ELocked(Exception):
    def __init__(self, lock):
        self.lock = lock

class EUnlocked(Exception):
    pass

'''
Manages the lock hierarchy

Primitive implementation, there is no cleanup of metadata since there is not
memory restriction in the project.
'''
# TODO: Upgrading from shared to exclusive locks
# TODO: Probably not quite thread safe
class LockManager(object):
    def __init__(self):
        self._tables = dict()

    '''
    Check if a table is locked. If it doesn't have a lock, create one. Throw
    ELocked if locked and incompatible.
    '''
    def _checkTable(self, ownerID, table, exclusive):
        if table in self._tables:
            if (self._tables[table].lock.isExclusive
                and ownerID not in self._tables[table].lock.owners):
                raise ELocked(self._tables[table].lock)
        else:
            self._tables[table] = LockTable(unlocked=True)

    '''
    Lock a record. If it doesn't have a lock, create one and lock it.
    Returns [tableLock, recordLock]
    '''
    def _lockRecord(self, ownerID, table, recordID, exclusive):
        self._checkTable(ownerID, table, exclusive)
        if recordID in self._tables[table].recordLocks:
            if self._tables[table].recordLocks[recordID].acquire(ownerID,
                                                                 exclusive):
                lock = self._tables[table].recordLocks[recordID]
            else:
                raise ELocked(self._tables[table].recordLocks[recordID])
        else:
            # New entry
            self._tables[table].recordLocks[recordID] = Lock(ownerID,
                                                             exclusive)
            lock = self._tables[table].recordLocks[recordID]
        return lock

    '''
    Lock a table; if it doesn't exist, add it to the lock table and lock it.
    Returns [tableLock]
    '''
    def _lockTable(self, ownerID, table, exclusive):
        self._checkTable(ownerID, table, exclusive)
        if self._tables[table].lock.acquire(ownerID, exclusive):
            return self._tables[table].lock
        else:
            raise ELocked(self._tables[table].lock)

    '''
    Lock a record, raising ELocked if already locked
    exclusive: boolean, if True, acquire an exclusive lock, else a shared lock
    Returns lock reference
    '''
    def acquireRecord(self, ownerID, recordID, table, exclusive=False):
        return self._lockRecord(ownerID, table, recordID, exclusive)

    '''
    Lock a table, raising ELocked if already locked
    exclusive: boolean, if True, acquire an exclusive lock, else a shared lock
    Returns lock reference
    '''
    def acquireTable(self, ownerID, table, exclusive=False):
        return self._lockTable(ownerID, table, exclusive)

'''
Represents a lock table for one table
'''
class LockTable(object):
    def __init__(self, ownerID=None, unlocked=False, exclusive=False):
        self.lock = Lock(ownerID=ownerID, locked=not unlocked,
                         exclusive=exclusive)
        if not unlocked:
            if self.lock.acquire(ownerID, exclusive) is False:
                raise RuntimeError()
        self.recordLocks = dict()

    def __str__(self):
        s = 'LockTable {}:\n'.format(id(self))
        for (r, locks) in self.recordLocks.items():
            s += '    {}: {}\n'.format(r, locks)
        return s

'''
Lock objects used by LockManager.

Not thread safe.
'''
class Lock(object):
    '''
    Initialize and acquire lock. The common case is to immediately acquire.
    '''
    def __init__(self, ownerID=None, exclusive=False, locked=True):
        self._owners = []
        if not locked:
            self._exclusive = False
            self._locked = False
            self._refCount = 0
        else:
            self._exclusive = exclusive
            self._locked = True
            self._owners.append(ownerID)
            self._refCount = 1

    def __str__(self):
        return 'Lock {}: LOCKED: {} EXCLUSIVE: {} REFCOUNT: {}'.format(
            id(self), self._locked, self._exclusive, self._refCount)

    @property
    def owners(self):
        return self._owners

    @property
    def isExclusive(self):
        return self._exclusive

    @property
    def isLocked(self):
        return self._locked

    def acquire(self, ownerID, exclusive=False):
        if self._locked:
            if self._exclusive and ownerID not in self._owners:
                return False
            else:
                self._refCount += 1
                self._owners.append(ownerID)
        else:
            self._exclusive = exclusive
            self._locked = True
            self._refCount = 1
            self._owners.append(ownerID)
        return True

    def release(self, ownerID):
        if not self._locked:
            return
        if self._refCount <= 0:
            return
        self._refCount -= 1
        self._owners.remove(ownerID)
        if self._refCount == 0:
            self._locked = False
            self._exclusive = False

    '''
    Upgrade a lock to exclusive.

    Since this class does not detect who is the caller, this can be easily
    misused.
    '''
    def upgrade(self):
        self._exclusive = True
