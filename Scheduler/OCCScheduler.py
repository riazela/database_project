# By Kadie Clancy
import Scheduler
from DataManager.DataManager import Record
from DataManager.DataManager import DataManager
from Logger.Logger import Logger
from TransactionManager.TransactionManager import command

class OCCScheduler(Scheduler.Scheduler):
    def __init__(self, datamanager, logger):
        super(OCCScheduler, self).__init__(datamanager, logger)
        # Writes in the form of [Tid, table, recordID]
        self.schedulerWHistory = []
        # list of transaction objects
        self.activeTransactions = []
        self.logger = logger
        self.datamanager = datamanager

    '''
        Execute the command based on the command type
    '''
    def execute(self, command):
        # Find transaction object
        if command.commandtype != 'B':
            for t in self.activeTransactions:
                if t.transactionID == command.transactionId:
                    transaction = t

        if 'B' == command.commandtype:
            return self.begin(command)
        elif 'R' == command.commandtype:
            return self.read(transaction, command)
        elif 'W' == command.commandtype:
            return self.write(transaction, command)
        elif 'M' == command.commandtype:
            return self.multipleRead(transaction, command)
        elif 'D' == command.commandtype:
            return self.delete(transaction, command)
        elif 'C' == command.commandtype:
            return self.commit(transaction, command)
        elif 'A' == command.commandtype:
            return self.abort(transaction, command)
        else:
            raise RuntimeError('Invalid command.commandType')

    '''
        Begin Commands: create a new transaction object and append it to the list of
        activeTransactions, log the command, and return result
        def __init__(self, successful, record, restartId):
    '''
    def begin(self, command):
        newTransaction = TransactionOCC(self._newTID(), self.schedulerWHistory)
        self.activeTransactions.append(newTransaction)
        command.transactionId = newTransaction.transactionID
        self.logger.logCommand(command)
        #print(newTransaction.transactionID)
        return Scheduler.Result(True, None, newTransaction.transactionID)

    '''
        Read Commands: perform the read, append the read to the read list for the
        transaction, log the read and return the result
    '''
    def read(self, transaction, command):
        record = self.datamanager.getRecordById(command.tableName, command.recordId, command.transactionId)
        if record != None:
            transaction.readList.append([str(command.transactionId), command.tableName, record.id])
            self.logger.logCommand(command)
        else:
            transaction.readFromBuffer = True
        return Scheduler.Result(True, record)

    '''
        MRead Commands: perform the read, append the read(s) to the read list for the
        transaction, log the read and return the result
    '''
    def multipleRead(self, transaction, command):
        records = self.datamanager.getRecordByPhone(command.tableName,
                                                        command.recordPhone, command.transactionId)
        for x in records:
            transaction.readList.append([str(command.transactionId), command.tableName, x.id])
        self.logger.logCommand(command)
        return Scheduler.Result(True, records)

    '''
        Write Commands: add the command to the write buffer, return the result to result
    '''
    def write(self, transaction, command):
        transaction.writeBuffer.append(command)
        return Scheduler.Result(True, None)

    '''
        Delete Commands: treat like writes, add command to write buffer, return the result
    '''
    def delete(self, transaction, command):
        transaction.writeBuffer.append(command)
        return Scheduler.Result(True, None)

    '''
        Commit: compare the read list for the transaction to the SchedulerWHistory to see
        if anything has written to any of those values since the transaction read
        if so - abort
        else - run those write/delete commands in the buffer, delete transaction obj from 
            activeTranctions
    '''
    def commit(self, transaction, command):
        # Check for read conflicts
        flag = True
        if transaction.readFromBuffer == True:
            self.restart(transaction)
            flag = False
        else:
            for x in transaction.readList:
                for j in self.schedulerWHistory:
                    if (int(j[0]) < transaction.lastWrite) and (str(j[1]) == str(x[1])) and (str(j[2]) == str(x[2])):
                        self.restart(transaction)
                        flag = False

        if command.commandMode == 0:
            flag = True

        # If no conflicts
        if flag:
            #print(transaction.writeBuffer)
            for c in transaction.writeBuffer:
                #print(c)
                if 'W' == str(c.commandtype):
                    self.datamanager.write(c.tableName,
                                            Record(c.recordId, c.recordPhone,c.recordClientName), c.transactionId)
                    self.schedulerWHistory.append([int(c.transactionId), c.tableName, c.recordId])
                    self.logger.logCommand(c)
                elif 'D' == str(c.commandtype):
                    self.datamanager.delete(c.tableName, c.transactionId)
                    self.schedulerWHistory.append([int(c.transactionId), c.tableName, c.recordId])
                    self.logger.logCommand(c)
            self.logger.logCommand(command)
        return Scheduler.Result(True, None)


    '''
        Abort: abort based on command
    '''
    def abort(self, transaction, command):
        tid = transaction.transactionID
        transaction.remove()
        self.logger.logCommand(command)
        return Scheduler.Result(True, None)

    '''
        Restart: abort and restart based on conflict
    '''
    def restart(self, transaction):
        tid = transaction.transactionID
        transaction.remove()
        print "restart"
        return Scheduler.Result(True, None, None, tid)

    def beginTransaction(self):
        pass

# Keeps track of local write buffers, reads by the transaction, and the last
# transaction that has committed before it started to read
class TransactionOCC(object):
    def __init__(self, transactionID, writeHistory):
        self.transactionID = transactionID
        self.writeHistory = writeHistory
        # list of commands
        self.writeBuffer = []
        # of the form [Tid, Table, recordID]
        self.readList = []
        self.readFromBuffer = False
        if len(self.writeHistory) != 0:
            for x in self.writeHistory:
                self.lastWrite = self.writeHistory[:-1][0]
        else:
            self.lastWrite = 0

    def remove(self):
        self.transactionID = None
        # list of commands
        self.writeBuffer = []
        # of the form [Tid, Table, recordID]
        self.readList = []