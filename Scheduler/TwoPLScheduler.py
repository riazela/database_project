# Two-phase Locking Scheduler
#
# Limitations:
# - Can detect deadlocks, but is unable to notify the TM, so an exception is
#   raised.
# - Uses the DM directly, so there is no local caching.
# - Processes handle locks involving writes the same as transactions.
#
# Copyright (C) 2017 Patrick Gauvin
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
# USA
import Scheduler
import LockManager
import DataManager.Record

class TwoPLScheduler(Scheduler.Scheduler):
    def __init__(self, dataManager, logger):
        super(TwoPLScheduler, self).__init__(dataManager, logger)
        self._lockManager = LockManager.LockManager()
        self._waitsForGraph = WaitsForGraph()
        self._transactions = dict()

    def execute(self, command):
        # workaround until update TM to use strings for IDs
        if command.transactionId != -1:
            transaction = self._transactions[str(command.transactionId)]
        else:
            transaction = None
        if 'B' == command.commandtype:
            return self._begin(transaction, command)
        elif 'R' == command.commandtype:
            return self._read(transaction, command)
        elif 'W' == command.commandtype:
            return self._write(transaction, command)
        elif 'M' == command.commandtype:
            return self._multipleRead(transaction, command)
        elif 'D' == command.commandtype:
            return self._delete(transaction, command)
        elif 'C' == command.commandtype:
            return self._commit(transaction, command)
        elif 'A' == command.commandtype:
            return self._abort(transaction, command)
        else:
            raise RuntimeError('Invalid command.commandtype: "{}"'.format(
                               command.commandtype))

    def _begin(self, transaction, command):
        # TODO: differentiate between transactions and processes
        tID = str(self._newTID())
        self._transactions[tID] = Transaction(tID, command.commandMode == 0)
        self._waitsForGraph.add(tID)
        self._logger.logCommand(command)
        return Scheduler.Result(True, None, int(tID))

    def _read(self, transaction, command):
        # Processes do not care about locks for reading
        if (not transaction.isProcess
            and not transaction.checkRecordLock(command.tableName,
                                                command.recordId, False)):
            try:
                lock = self._lockManager.acquireRecord(transaction.transactionID,
                                                       command.recordId,
                                                       command.tableName)
            except LockManager.ELocked as e:
	    	try:
			[self._waitsForGraph.addEdge(t, transaction.transactionID)
			    for t in e.lock.owners]
			return Scheduler.Result(False, None)
		except EDeadlock:
			self._abort(transaction, command)
			return Scheduler.Result(False, None, -1, int(transaction.transactionID))
            transaction.addRecordLock(command.tableName, command.recordId,
                                      lock)
        record = self._dataManager.getRecordById(command.tableName,
                                                 command.recordId,
                                                 int(transaction.transactionID))
        self._logger.logCommand(command)
        return Scheduler.Result(True, record)

    def _write(self, transaction, command):
        # TODO: Handle lock upgrades to exclusive
        # TODO: Processes cannot corrupt a transaction
        if not transaction.checkRecordLock(command.tableName,
                                           command.recordId, True):
            try:
                lock = self._lockManager.acquireRecord(transaction.transactionID,
                                                       command.recordId,
                                                       command.tableName,
                                                       True)
            except LockManager.ELocked as e:
		try:
			[self._waitsForGraph.addEdge(t, transaction.transactionID)
			    for t in e.lock.owners]
			return Scheduler.Result(False, None)
		except EDeadlock:
			self._abort(transaction, command)
			return Scheduler.Result(False, None, -1, int(transaction.transactionID))
            transaction.addRecordLock(command.tableName, command.recordId,
                                      lock)
        self._dataManager.write(command.tableName,
                                DataManager.Record.Record(command.recordId,
                                                          command.recordPhone,
                                                          command.recordClientName),
                                int(transaction.transactionID))
        self._logger.logCommand(command)
        return Scheduler.Result(True, None)

    def _multipleRead(self, transaction, command):
        if (not transaction.isProcess
            and not transaction.checkTableLock(command.tableName, False)):
            try:
                lock = self._lockManager.acquireTable(transaction.transactionID,
                                                      command.tableName)
            except LockManager.ELocked as e:
		try:
			[self._waitsForGraph.addEdge(t, transaction.transactionID)
			    for t in e.lock.owners]
			return Scheduler.Result(False, None)
		except EDeadlock:
			self._abort(transaction, command)
			return Scheduler.Result(False, None, -1, int(transaction.transactionID))
            transaction.addTableLock(command.tableName, lock)
        records = self._dataManager.getRecordByPhone(command.tableName,
                                                     command.recordPhone,
                                                     transaction.transactionID)
        self._logger.logCommand(command)
        return Scheduler.Result(True, records)

    def _delete(self, transaction, command):
        # TODO: Handle lock upgrades to exclusive
        # TODO: Processes cannot interrupt transactions
        if not transaction.checkTableLock(command.tableName, True):
            try:
                lock = self._lockManager.acquireTable(transaction.transactionID,
                                                      command.tableName, True)
            except LockManager.ELocked as e:
		try:
			[self._waitsForGraph.addEdge(t, transaction.transactionID)
			    for t in e.lock.owners]
			return Scheduler.Result(False, None)
		except EDeadlock:
			self._abort(transaction, command)
			return Scheduler.Result(False, None, -1, int(transaction.transactionID))
            transaction.addTableLock(command.tableName, lock)
        self._dataManager.delete(command.tableName,
                                 int(transaction.transactionID))
        self._logger.logCommand(command)
        return Scheduler.Result(True, None)

    def _commit(self, transaction, command):
        transaction.releaseAllLocks()
        self._waitsForGraph.remove(transaction.transactionID)
        del self._transactions[transaction.transactionID]
        # TODO: Cleanup cache references
        self._logger.logCommand(command)
        return Scheduler.Result(True, None)

    def _abort(self, transaction, command):
        self._rollback(transaction)
        transaction.releaseAllLocks()
        self._waitsForGraph.remove(transaction.transactionID)
        del self._transactions[transaction.transactionID]
        # TODO: Cleanup cache references
        self._logger.logCommand(command)
        return Scheduler.Result(True, None)

    def _rollback(self, transaction):
        commands = self._logger.getRollbackCommands(transaction.transactionID)
        for c in commands:
            result = self.execute(c)
            if not result.successful:
                raise RuntimeError('Rollback failed')

class EDeadlock(Exception):
    def __init__(self, t1, t2):
        self.t1 = t1
        self.t2 = t2

    def __str__(self):
        return "Deadlock detected between transaction {} and {}.".format(
            self.t1, self.t2)

class WaitsForGraph(object):
    def __init__(self):
        # Nodes will be entries in the dictionary whose value is a list of
        # directed edges to which nodes it connects to.
        self._graph = dict()

    '''
    Searches for cycles, raising EDeadlock if one is found.
    '''
    def _checkDeadlock(self):
        result = self._detectCycle()
        if result[0]:
	    print('Detected deadlock:')
	    print(EDeadlock(*result[1:]))
            raise EDeadlock(*result[1:])

    '''
    Returns (success, node, node) where success is a boolean
    '''
    def _detectCycle(self):
        use_map = []
        for node in self._graph.keys():
            result = self._findCycle(node, [], use_map)
            if result[0]:
                return result
        return (False, None, None)

    def _findCycle(self, current, currentPath, used):
        # Depth-first traversal, checks for cycles on the current path,
        # marking nodes that have been seen in used

        # Ignore nodes that have been visited, they will not lead to a cycle
        if current not in used:
            if current in currentPath:
                return (True, currentPath.pop(), current)
            currentPath.append(current)
            for node in self._graph[current]:
                result = self._findCycle(node, currentPath, used)
                if result[0]:
                    return result
            used.append(current)
            currentPath.pop()
        return (False, None, None)

    '''
    Add an edge from t1 to t2.

    Throws exception with transaction ids if deadlock detected
    '''
    def addEdge(self, t1, t2):
        if t1 == t2:
            raise RuntimeError('Attempted to created edge to self')
        self._graph[t1].append(t2)
        self._checkDeadlock()

    '''
    Add a transaction.
    '''
    def add(self, transaction):
        self._graph[transaction] = []

    '''
    Remove a transaction and all edges to it.
    '''
    def remove(self, transaction):
        del self._graph[transaction]
        for node in self._graph.values():
            while transaction in node:
                node.remove(transaction)

class Transaction(object):
    def __init__(self, transactionID, isProcess):
        self.transactionID = transactionID;
        self.isProcess = isProcess
        # store the obtained locks here
        self._lockTable = dict()

    def __str__(self):
        s = 'Transaction {}\n'.format(id(self))
        for (table, lt) in self._lockTable.items():
            s += '  {}: {}\n'.format(table, lt)
        return s

    # TODO: Handle exclusive
    def checkTableLock(self, table, exclusive):
        try:
            if self._lockTable[table].lock is not None:
                return True
            else:
                return False
        except(KeyError):
            return False

    # TODO: Handle exclusive
    '''
    Check if the transaction holds a lock on a record.
    '''
    def checkRecordLock(self, table, recordID, exclusive):
        try:
            if recordID in self._lockTable[table].recordLocks:
                return True
            else:
                return False
        except(KeyError):
            return False

    # TODO: Support upgrading lock
    def addTableLock(self, table, lock):
        if table not in self._lockTable:
            self._lockTable[table] = LockManager.LockTable(unlocked=True)
        self._lockTable[table].lock = lock

    # TODO: Support upgrading lock
    def addRecordLock(self, table, recordID, lock):
        if table not in self._lockTable:
            self._lockTable[table] = LockManager.LockTable(unlocked=True)
        if recordID in self._lockTable[table].recordLocks:
            raise RuntimeError()
        self._lockTable[table].recordLocks[recordID] = lock

    def releaseAllLocks(self):
        for t in self._lockTable.values():
            for l in t.recordLocks.values():
                l.release(self.transactionID)
            if t.lock is not None:
                t.lock.release(self.transactionID)

'''
class Cache(object):
    def __init__(self):
        # Dictionary with tablenames as keys. The values are dictionaries of
        # records with the recordID as keys
        self._cache = dict()

    def insertRecord(self, table, record):
        self._cache[table].records[record.recordId] = record

    def removeRecord(self, table, recordID):
        del self._cache[table].records[recordID]

    def flushToDM(self, dataManager):
        for (tableName, table) in self._cache.items():
            for (recordName, record) in table.items()
                if record.dirty:
                    dataManager.write(tableName, record.record,
                                      record.transactionID)

class CacheRecord(object):
    def __init__(self, initalOwner, table, record):
        self.table = table
        self.record = record
        self.dirty = False
        # This only makes sense for records that are written to, since there
        # should be one writer at a time.
        self.transactionID = initialOwner
'''
