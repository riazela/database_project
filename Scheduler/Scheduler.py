# Scheduler abstract class and Result class
#
# Copyright (C) 2017 Patrick Gauvin
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
# USA
import abc

class Scheduler(object):
    __metaclass__ = abc.ABCMeta
    # Transaction IDs are allocated across all children
    _lastTID = 0

    def __init__(self, dataManager, logger):
        self._dataManager = dataManager
        self._logger = logger

    @abc.abstractmethod
    def execute(self, command):
        pass

    def _newTID(self):
        # Primitive implementation, Python has no limit on integer size
        new = Scheduler._lastTID + 1
        Scheduler._lastTID = new
        return new

'''
Result objects returned to the transaction manager.
'''
class Result(object):
    def __init__(self, successful, record, transactionId=-1, restartId=-1):
        self.successful = successful
        self.record = record
        self.transactionId = transactionId
        self.restartId = restartId

    def __str__(self):
        return 'Result {}: \n  successful: {}\n  record: {}\n  transactionId: {}\n  restartId: {}'.format(
            id(self), self.successful, self.record, self.transactionId,
            self.restartId)
