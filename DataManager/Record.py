# By Alireza Samadian
class Record():
    def __init__(self,id,phone,name):
        self.id = id
        self.phone=phone
        self.name = name

    def __str__(self):
        return 'ID: {} Phone: {} Name: {}'.format(self.id, self.phone,
                                                  self.name)
