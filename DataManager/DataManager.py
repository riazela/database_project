# By Alireza Samadian
from abc import abstractmethod
import struct
from Record import Record
import os

class DataManagerInterface():
    # it will return a record if it is found.
    # it will return None if there isn't such a record or table
    @abstractmethod
    def getRecordById(self, table_name, id, transaction_id): raise NotImplementedError
    # it returns list of records
    # it returns empty list [] if there isn't such a phone prefix or table
    @abstractmethod
    def getRecordByPhone(self, table_name, phone, transaction_id): raise NotImplementedError

    @abstractmethod
    def write(self, table_name, record, transaction_id): raise NotImplementedError
    @abstractmethod
    def delete(self, table_name, transaction_id): raise NotImplementedError

    @abstractmethod
    def delete_record(self, table_name, record, transaction_id): raise NotImplementedError


def int_to_bytes(n):
    return list(struct.pack(">I", n))

def bytes_to_int(s):
    st = ''.join(s)
    return int(st.encode('hex'), 16)

class DataManager(DataManagerInterface):
    def __init__(self,logger):
        self.logger = logger
        self.cache = Cache(logger)
        self.number_or_writes = 0
        self.number_or_reads = 0

    def hash_id(self,id):
        return id%16
    '''
        getRecordById returns the record object associated with the given id and if it doesn't find it will return None
    '''
    def getRecordById(self, table_name, id, transaction_id):
        # type: (str, str, int) -> Record
        self.number_or_reads += 1
        try:
            page = self.cache.get_page(table_name,self.hash_id(id))
            while True:
                rec_index = page.find_index_by_id(id);
                if rec_index>-1:
                    record = page.get_record_by_index(rec_index)
                    self.logger.logRead( transaction_id, table_name, record)
                    return record
                else:
                    if page.next_page>0:
                        page = self.cache.get_page(table_name,page.next_page)
                    else:
                        return None
        except Exception:
            return None


    '''
        getRecordByPhone returns the record objects associated with the given phonePrefix
        and if it doesn't find any it will return []
    '''
    def getRecordByPhone(self, table_name, phone, transaction_id):

        # type: (str, str, int) -> list
        try:
            result = []
            for i in range(0,16):
                page = self.cache.get_page(table_name, i)
                while True:
                    indices = page.find_index_by_phone_prefix(phone)
                    for j in indices:
                        result = result + [page.get_record_by_index(j)]
                    if page.next_page>0:
                        page = self.cache.get_page(table_name,page.next_page)
                    else:
                        break
            self.logger.logMRead( transaction_id, table_name, result)
            self.number_or_reads += len(result)
            return result
        except Exception:
            return []

    '''
        It inserts a record. If there is a record with the same id it will update.
    '''
    def write(self, table_name, record, transaction_id):
        # type: (str, Record, int) ->
        self.number_or_writes+=1
        page = self.cache.get_or_create_page(table_name, self.hash_id(record.id))
        rec_index=-1
        while True:
            rec_index = page.find_index_by_id(id)
            if rec_index>-1:
                break
            else:
                if page.number_of_records>=page.maximum_records and page.next_page>0:
                    page = self.cache.get_page(table_name,page.next_page)
                else:
                    rec_index=-1
                    break

        if rec_index>-1:
            prev_record = page.get_record_by_index(rec_index)
            page.write_record(rec_index,record)
            self.logger.logUpdate(transaction_id,table_name,prev_record,record)
        else:
            if page.number_of_records>=page.maximum_records:
                new_page_number = self.__get_new_page__(table_name)
                page.set_next_page(new_page_number)
                page = self.cache.get_or_create_page(table_name,new_page_number)
                page.write_record(page.number_of_records, record)
            else:
                page.write_record(page.number_of_records,record)
            self.logger.logUpdate(transaction_id,table_name,None,record)

    def __get_new_page__(self,table_name):
        try:
            f = open(table_name + '.info', 'r+b')
        except:
            f = open(table_name + '.info', 'w+b')
        f.seek(0)
        a = f.read()
        if (len(a)<4):
            result = 16
        else:
            result = bytes_to_int(a)
        a = int_to_bytes(result+1)
        f.seek(0)
        f.write(''.join(a))
        f.close()
        return result
    '''
        it deletes the entire table and returns a list of all records in that table
    '''
    def delete(self, table_name, transaction_id):
        self.number_or_writes += 1
        try:
            f = open(table_name + '.info', 'r+b')
        except:
            f = open(table_name + '.info', 'w+b')
        f.seek(0)
        a = f.read()
        if (len(a) < 4):
            number_of_pages = 16
        else:
            number_of_pages = bytes_to_int(a)
        f.close()
        all_records=[]
        try:
            for i in range(1,number_of_pages):
                all_records += self.cache.get_page(table_name,i).get_all_records()
        except:
            pass
        self.cache.swap_out_pages(table_name)
        self.__remove_file_if_exists__(table_name+'.info')
        self.__remove_file_if_exists__(table_name+'.db')
        for i in all_records:
            self.logger.logUpdate(transaction_id, table_name, i, None)
        return all_records



    def __remove_file_if_exists__(self,filename):
        try:
            os.remove(filename)
        except OSError:
            pass


    def delete_record(self, table_name, record_id, transaction_id):
        self.number_or_writes += 1
        rec_index=-1
        try:
            page = self.cache.get_page(table_name,self.hash_id(record_id))
            while True:
                rec_index = page.find_index_by_id(record_id)
                if rec_index>-1:
                    break
                else:
                    if page.next_page>0:
                        page = self.cache.get_page(table_name,page.next_page)
                    else:
                        break
        except Exception:
            rec_index = -1
            page = None

        if rec_index > -1:
            record = page.get_record_by_index(rec_index)
            self.logger.logUpdate(transaction_id, table_name, record, None)
            page.delete_record_by_index(rec_index)

    def write_all_pages(self):
        f = open('read_write.log', 'w')
        f.write("number of reads: "+str(self.get_number_or_reads()))
        f.write("\n")
        f.write("number of writes: "+str(self.get_number_or_writes()))
        f.close()
        self.cache.write_all_pages()

    def get_number_or_reads(self):
        return self.number_or_reads

    def get_number_or_writes(self):
        return self.number_or_writes


class Page:
    page_size = 2048
    maximum_records = 63
    def __init__(self,input_bytes,filename,page_number,time_stamp):
        self.__bytes__ = list(input_bytes)
        self.filename = filename
        self.last_touch = time_stamp
        self.page_number = page_number
        self.is_dirty = False
        self.__read_meta_data__()

    def __read_meta_data__(self):
        data = self.__bytes__
        self.number_of_records = bytes_to_int(''.join(data[0:4]))
        self.next_page = bytes_to_int(''.join(data[4:8]))

    def set_next_page(self,page_id):
        self.next_page = page_id
        self.__write_meta_data__()

    def __write_meta_data__(self):
        self.__bytes__[0:4] = list(int_to_bytes(self.number_of_records))
        self.__bytes__[4:8] = list(int_to_bytes(self.next_page))
        self.is_dirty = True

    def touch(self,timestamp):
        self.last_touch=timestamp

    def get_bytes(self):
        return self.__bytes__

    def set_bytes(self,input_bytes):
        self.__bytes__ = list(input_bytes)
        self.is_dirty = True

    def find_index_by_id(self,id):
        j = Page.page_size
        for i in range(0,self.number_of_records):
            j -= 4
            rec_id = bytes_to_int(self.__bytes__[j:j+4])
            if (rec_id==id):
                return i
        return -1

    def find_index_by_phone_prefix(self,phone_prefix):
        result = []
        j = 32
        for i in range(0,self.number_of_records):
            phone = ''.join(self.__bytes__[j:j+12])
            if (phone_prefix[0:3]==phone[0:3]):
                result.append(i)
            j += 12 + 16
        return result

    def get_record_by_index(self,index):
        if (index>=self.number_of_records):
            raise Exception('index is greater than the number of records in the page')
        if (index<0):
            raise Exception('index is negative')
        j = Page.page_size-4*index-4
        rec_id = bytes_to_int(self.__bytes__[j:j + 4])
        j = 32+index*(12+16)
        client_phone = ''.join(self.__bytes__[j:j+12])
        j+=12
        client_name = ''.join(self.__bytes__[j:j+16])
        return Record(rec_id,client_phone,client_name)

    def delete_record_by_index(self, index):
        if (index>=self.number_of_records):
            raise Exception('index is greater than the number of records in the page')
        if (index<0):
            raise Exception('index is negative')
        for i in range(index,self.number_of_records-1):
            j = 32 + (12+16)*i
            jp = 32 + (12+16)*(i+1)
            self.__bytes__[j:j+(12+16)] = self.__bytes__[jp:jp+(12+16)]
            j = self.page_size - 4 - 4 * i
            jp = self.page_size - 4 - 4 * (i+1)
            self.__bytes__[j:j + 4] = self.__bytes__[jp:jp + 4]
        self.number_of_records-=1
        self.__write_meta_data__()

    def is_page_full(self):
        return self.number_of_records>=Page.page_size

    def get_all_records(self):
        result = []
        for i in range(0,self.number_of_records):
            result = result + [self.get_record_by_index(i)]
        return result

    def write_record(self,index,record):
        if (index>self.number_of_records):
            raise Exception('index is incorrect')
        if (index>=Page.maximum_records):
            raise Exception('index is greater than the maximum number of possible records in each page')
        if (index<0):
            raise Exception('index is negative')
        self.is_dirty=True
        if (index==self.number_of_records):
            self.number_of_records+=1
            self.__write_meta_data__()
        j = 32 + (12+16)*index
        self.__bytes__[j:j+min(len(record.phone),12)] = list(record.phone)
        j += 12
        self.__bytes__[j:j+min(len(record.name),16)] = list(record.name)
        j = self.page_size-4-4*index
        self.__bytes__[j:j+4] = int_to_bytes(record.id)

    def __str__(self):
        print "Page: FileName: " + str(self.filename) + " Last Touch:" + str(self.last_touch) + " page_number: " + str(self.page_number) + " is_dirty: " + str(self.is_dirty)


class Cache:
    cache_size = 60
    def __init__(self,logger):
        self.pages = []
        self.logger = logger
        self.time_stamp = 0

    def get_page(self,filename,page_number):
        for i in self.pages:
            if i.filename==filename and i.page_number==page_number:
                return i

        p = self.read_page_from_disk(filename, page_number)
        self.swap_in_page(p)
        return p

    def swap_in_page(self,new_page):
        if (len(self.pages)<Cache.cache_size):
            self.pages.append(new_page)
        else:
            min = 0
            for i in range(1,len(self.pages)):
                if (self.pages[i].last_touch<self.pages[min].last_touch):
                    min = i
            if self.pages[min].is_dirty:
                self.write_page_to_disk(self.pages[min])
            self.logger.logSwap( "swap out", self.pages[min].filename, new_page.page_number, 0)
            self.pages[min] = new_page

        self.logger.logSwap( "swap in", new_page.filename, new_page.page_number, 0)

    def get_or_create_page(self,filename,page_number):
        try:
            p = self.get_page(filename,page_number)
        except Exception:
            data = []
            for i in range(0,Page.page_size):
                data.append('\x00')
            self.time_stamp+=1
            p = Page(data,filename,page_number,self.time_stamp)
            self.swap_in_page(p)
        return p

    def read_page_from_disk(self,filename,page_number):
        f = open(filename+'.db','r+b')
        f.seek(page_number*Page.page_size)
        data = f.read(Page.page_size)
        f.close()
        self.time_stamp += 1
        if len(data)<Page.page_size:
            data = ''
            for i in range(0,Page.page_size):
                data = data.join('\x00')
        return Page(data,filename,page_number,self.time_stamp)


    def write_page_to_disk(self,page):
        page.is_dirty = False
        try:
            f = open(page.filename+'.db','r+b')
        except:
            f = open(page.filename + '.db', 'w+b')
        f.seek(page.page_number * Page.page_size)
        f.write(''.join(page.get_bytes()))
        f.close()
        self.time_stamp += 1

    def write_all_pages(self):
        for i in self.pages:
            if i.is_dirty:
                self.write_page_to_disk(i)

    def swap_out_pages(self,table_name):
        new_list_of_pages = []
        for i in self.pages:
            if i.filename==table_name:
                if i.is_dirty:
                    self.write_page_to_disk(i)
            else:
                new_list_of_pages += [i]
        self.pages = new_list_of_pages

    def __str__(self):
        print "Cache: Number of pages: " + str(len(self.pages)) + " size: " + str(self.cache_size) + " current time stamp:  " + str(self.time_stamp)
        for p in self.pages:
            print p