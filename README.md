# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* ##### Quick summary

    This is the project for CS2550. It is written in python. We used bitbucket for our git repository. 
    This is our [Repository](https://bitbucket.org/riazela/database_project)

* ##### Version
    1.0.0

## Set up and run 

* ##### Summary of set up

    Since it is written in python there is no need to setup.

* ##### Dependencies

    Python 2.7

* ##### How to run

    In windows: 
    py main.py -d DIRECTORY -s SEED --executionType EXECUTIONTYPE
    
    DIRECTORY           Path to the directory containing the script files to
                        be processed by the system.
    
    SEED                The seed of the random number generator to be used by
                        the system
    
    EXECUTIONTYPE       The execution type; 1 for round robin, 0 for random

*  ##### Output

    The output is the requested statistics. 
    You may also find total number of read and writes in read_write.log.
    Log is generated in log.json and the result of reading an writings are there.


## About us 

* Repo owner

    Team 4

* Team contact
    * Kadie Clancy kdc42@pitt.edu
    * Patrick Gauvin pgg4@pitt.edu
    * Alireza Samadianzakaria als417@pitt.edu
    * Sumedha Singla sus98@pitt.edu

### Contributions ###

    * DataManager/Datamanager.py                  Alireza Samadianzakaria
    * Datamanager/Record.py                       Alireza Samadianzakaria
    * Logger/Logger.py                            Kadie Clancy
    * Scheduler/LockManager.py                    Patrick Gauvin
    * Scheduler/OCCScheduler.py                   Kadie Clancy
    * Scheduler/Scheduler.py                      Patrick Gauvin
    * Scheduler/TwoPLScheduler.py                 Patrick Gauvin
    * TransactionManager/TransactionManager.py    Sumedha Singla