# Tests that the waits-for-graph works properly
#
# Copyright (C) 2017 Patrick Gauvin
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
# USA
import Scheduler.TwoPLScheduler

class test(Scheduler.TwoPLScheduler.WaitsForGraph):
    def testDetect(self):
        print('Begin deadlock tests...')

        self._graph = {'a': ['b'], 'b': ['c'], 'c': []}
        self._checkDeadlock()
        print('OK')

        self._graph = {'d': ['b', 'c'], 'b': ['c'], 'c': []}
        self._checkDeadlock()
        print('OK')

        self._graph = {'a': ['b'], 'b': ['a'], 'c': []}
        try:
            self._checkDeadlock()
        except Scheduler.TwoPLScheduler.EDeadlock as e:
            print('Expected failure:')
            print(e)
        else:
            print('UNEXPECTED SUCCESS')

        self._graph = {'a': ['b'], 'b': ['c'], 'c': ['a']}
        try:
            self._checkDeadlock()
        except Scheduler.TwoPLScheduler.EDeadlock as e:
            print('Expected failure:')
            print(e)
        else:
            print('UNEXPECTED SUCCESS')

        self._graph = {'a': ['b'], 'b': ['c'], 'c': ['b']}
        try:
            self._checkDeadlock()
        except Scheduler.TwoPLScheduler.EDeadlock as e:
            print('Expected failure:')
            print(e)
        else:
            print('UNEXPECTED SUCCESS')

if __name__ == '__main__':
    test().testDetect()
