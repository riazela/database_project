# Two-phase Locking Scheduler simple tests
#
# Was used before and during integration.
#
# Copyright (C) 2017 Patrick Gauvin
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
# USA
import Scheduler.TwoPLScheduler
import DataManager.DataManager as DataManager
import Logger.Logger as Logger

'''
# Fake DataManager
class DataManager(object):
    def getRecordById(self, table, rID):
        print('DM: getRecorById: {}, {}'.format(table, rID))

    def getRecordByAreaCode(self, table, phone):
        print('DM: getRecorByAreaCode: {}, {}'.format(table, phone))

    def write(self, table, record):
        print('DM: write: {}, {}'.format(table, record))
'''

# Simple Command class
class Command(object):
    def __init__(self, commandMode, commandType, tableName, recordID,
        recordClientName, recordPhone):
        self.commandMode = commandMode
        self.commandtype = commandType
        self.tableName = tableName
        self.recordId = recordID
        self.recordClientName = recordClientName
        self.recordPhone = recordPhone
        self.transactionId = -1

    def __str__(self):
        return '{}'.format(self.commandtype)

def run(s, transaction):
    print('\n>>>New transaction')
    r = s.execute(transaction[0])
    tID = r.transactionId
    print(r)
    for command in transaction[1:]:
        command.transactionId = tID
        print('\nStarting command: {}'.format(command))
        r = s.execute(command)
        print(r)
    if not r.successful:
        raise RuntimeError()

def run_interleaved(s, transactions):
    print('\n>>>New interleaved transaction')
    tIDs = []
    # Execute the begin command for each
    for t in transactions:
        r = s.execute(t[0])
        tID = r.transactionId
        for command in t[1:]:
            command.transactionId = tID
    # Create list of interleaved commands
    i = 1
    final = []
    while i < max(map(len, transactions)):
        for t in transactions:
            if i < len(t):
                final.append(t[i])
        i += 1

    halted = []
    for command in final:
        if command.transactionId not in halted:
            print('\nStarting command (tID {}): {}'.format(command.transactionId,
                                                           command))
            r = s.execute(command)
            print(r)
            if not r.successful:
                print('FAIL, halting tID {}'.format(command.transactionId))
                halted.append(command.transactionId)

if __name__ == '__main__':
    # Spoof logger and DM for debugging
    #logger = None
    #dm = DataManager()

    logger = Logger.Logger()
    dm = DataManager.DataManager(logger)
    s = Scheduler.TwoPLScheduler.TwoPLScheduler(dm, logger)
    read_only = [ 
        Command(1, 'B', None, None, None, None),
        Command(1, 'R', 'table1', 1, None, None),
        Command(1, 'R', 'table1', 2, None, None),
        Command(1, 'R', 'table1', 3, None, None),
        Command(1, 'C', None, None, None, None)
    ]
    rw_no_conflict = [ 
        Command(1, 'B', None, None, None, None),
        Command(1, 'R', 'table1', 1, None, None),
        Command(1, 'R', 'table1', 2, None, None),
        Command(1, 'W', 'table2', 3, 'Squidward', '123-456-7890'),
        Command(1, 'C', None, None, None, None)
    ]
    rw_no_conflict_abort = [ 
        Command(1, 'B', None, None, None, None),
        Command(1, 'R', 'table1', 1, None, None),
        Command(1, 'R', 'table1', 2, None, None),
        Command(1, 'W', 'table2', 3, 'Squidward', '123-456-7890'),
        Command(1, 'A', None, None, None, None)
    ]
    rw_lock_upgrade = [ 
        Command(1, 'B', None, None, None, None),
        Command(1, 'R', 'table1', 1, None, None),
        Command(1, 'W', 'table1', 2, 'Squidward', '123-456-7890'),
        Command(1, 'R', 'table1', 3, None, None),
        Command(1, 'C', None, None, None, None)
    ]
    two_read_only = [
        [
            Command(1, 'B', None, None, None, None),
            Command(1, 'R', 'table1', 1, None, None),
            Command(1, 'R', 'table1', 2, None, None),
            Command(1, 'R', 'table1', 3, None, None),
            Command(1, 'C', None, None, None, None)
        ],
        [
            Command(1, 'B', None, None, None, None),
            Command(1, 'R', 'table1', 1, None, None),
            Command(1, 'R', 'table1', 2, None, None),
            Command(1, 'R', 'table1', 4, None, None),
            Command(1, 'C', None, None, None, None)
        ]
    ]
    two_rw = [
        [
            Command(1, 'B', None, None, None, None),
            Command(1, 'R', 'table1', 1, None, None),
            Command(1, 'W', 'table1', 2, 'Squidward', '123-456-7890'),
            Command(1, 'R', 'table1', 3, None, None),
            Command(1, 'C', None, None, None, None)
        ],
        [
            Command(1, 'B', None, None, None, None),
            Command(1, 'W', 'table1', 1, 'Squidward', '123-456-7890'),
            Command(1, 'R', 'table1', 2, None, None),
            Command(1, 'R', 'table1', 4, None, None),
            Command(1, 'C', None, None, None, None)
        ]
    ]
    run(s, read_only)
    run(s, rw_no_conflict)
    run(s, rw_no_conflict_abort)
    run(s, rw_lock_upgrade)
    run_interleaved(s, two_read_only)
    run_interleaved(s, two_rw)

    dm.write_all_pages()
