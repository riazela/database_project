'''
Author: Sumedha Singla

'''
import os
import random
import time
import pdb
# Defining a metaclass to specify a singleton class
class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class TransactionManager(object):
    #__metaclass__ = Singleton

    scriptCurrentLine = []  # Saves the line number with in the script which is to be processed next
    scriptList = []  # Its the list of scripts to be executed. Each script have a number of operations to be performed
    transactionIdForScript = []  # The transaction id alloted by the scheduler to the current script
    transactionModeForScript = [] #Save the mode transaction or process
    transactionScriptMap = {} #Its a hashmap. Key: Transaction ID Value (ScriptNumber, lineNumber InScript)
    noOfRestarts = 0
    committedTransaction = {} #Key: (ScriptNumber, lineNumber InScript) of committed transaction Value: commit time
    committedProcess = {}
    abortedProcess = {}
    abortedTransaction = {}
    avgTimeForOperationT = {"B": [0, 0], "C": [0, 0], "R": [0, 0], "M": [0, 0], "D": [0, 0], "A": [0, 0], "W": [0, 0]}
    avgTimeForOperationP = {"B": [0, 0], "C": [0, 0], "R": [0, 0], "M": [0, 0], "D": [0, 0], "A": [0, 0], "W": [0, 0]}
    transactionStartTime = {}
    transactionStat = {}
    switch = False



    def __init__(self, schedulerOCC, scheduler2PL, scriptDirectory, seed, executionType=1, restartThreshold = 10):
        print('Debug Info: In Transaction Manager constructor')
        self.SchedulerObject2PL = scheduler2PL
        self.SchedulerObject = schedulerOCC #default scheduler
        self.scriptDirectory = scriptDirectory
        self.executionType = executionType #by default its Round Robin. choose self.executionType = 0 for Random
        self.randomSeed = seed
        self.restartThreshold = restartThreshold

    '''
    This function will loop over all the script files in the script directory and 
    creates a local copy for each script.
    '''
    def transactionReader(self):
        for fileRoot, directories, files in os.walk(self.scriptDirectory):
            # get the number of script files
            nScriptFiles = len(files)
            # all the script files starts execution from line number 0
            self.scriptCurrentLine = [0 for i in range(0, nScriptFiles)]
            # since none of the scripts have started execution as of now. So their transaction id is -1
            self.transactionIdForScript = [-1 for i in range(0, nScriptFiles)]
            # since none of the scripts have started execution as of now. So their transaction id is -1
            self.transactionModeForScript = [-1 for i in range(0, nScriptFiles)]
            # Loop through all the script files in the script directory
            for f in files:
                scriptFilePath = os.path.join(fileRoot, f)
                with open(scriptFilePath, 'r') as file:
                    script = file.read()
                #list of operations in the current script
                script = script.split('\n')
                self.scriptList.append(script)
        if len(self.scriptList) != 0:
            print "Debug Info: Complete the reading of " + str(len(self.scriptList)) + " script files from the folder: " + self.scriptDirectory
            return True
        else:
            return False

    '''
    The below function starts the execution of the Transaction Manager.
    '''
    def execute(self):
        #Class the transaction reader to read all the script files
        if self.switch == False:
            print "Debug Info: Reading the input script files"
            readAllScript = self.transactionReader()
            if readAllScript == False:
                print "Error Message: There should be atleast one script to execute in directory: " + self.scriptDirectory
                return False

        if self.executionType == 0:
            flag = self.randomExecution()
        else:
            flag = self.roundRobinExecution()

        if self.switch == True:
            self.switchTo2PL()

        print "Debug Info: All the scripts have finished running"
        print "****************************************************************************************************"
        print "The stats for current execution:"
        print "The number of committed transactions: " + str(len(self.committedTransaction.keys()))
        print "The number of committed processes: " + str(len(self.committedProcess.keys()))
        print "The number of aborted transactions: " + str(len(self.abortedTransaction.keys()))
        print "The number of aborted processes: " + str(len(self.abortedProcess.keys()))
        print "****************************************************************************************************"
        committedT = [0, 0 ,0 ,0]
        abortedT = [0, 0 ,0 ,0]
        committedP = [0, 0 ,0 ,0]
        abortedP = [0, 0 ,0 ,0]

        for k in self.transactionStat:
            if self.committedTransaction.has_key(k):
                committedT = [x + y for x, y in zip(committedT, self.transactionStat[k])]
            elif self.abortedTransaction.has_key(k):
                abortedT = [x + y for x, y in zip(abortedT, self.transactionStat[k])]
            elif self.committedProcess.has_key(k):
                committedP = [x + y for x, y in zip(committedP, self.transactionStat[k])]
            elif self.abortedProcess.has_key(k):
                abortedP = [x + y for x, y in zip(abortedP, self.transactionStat[k])]

        print "Total Number of operations (R, M, W, D) for committed Transaction:" + str(committedT)
        print "Total Number of operations (R, M, W, D) for committed Processes:" + str(committedP)
        print "Total Number of operations (R, M, W, D) for aborted Transaction:" + str(abortedT)
        print "Total Number of operations (R, M, W, D) for aborted Processes:" + str(abortedP)
        print "****************************************************************************************************"

        print "No. Of restarts"
        print self.noOfRestarts
        print "The average execution time for each committed transaction"
        time = 0
        for k in self.committedTransaction.keys():
            time += self.committedTransaction[k]
        if len(self.committedTransaction.keys()) != 0:
            avgTime = time / len(self.committedTransaction.keys())
        else:
            avgTime = -1
        print avgTime
        print "****************************************************************************************************"
        print "The average response time of each operation in Transaction"
        for k in self.avgTimeForOperationT.keys():
            if self.avgTimeForOperationT[k][1] != 0:
                avgTime = self.avgTimeForOperationT[k][0]/self.avgTimeForOperationT[k][1]
                print "Operation: " + k + " Average response time: " + str(avgTime) + " in ms"
        print "The average response time of each operation in Process"
        for k in self.avgTimeForOperationP.keys():
            if self.avgTimeForOperationP[k][1] != 0:
                avgTime = self.avgTimeForOperationP[k][0] / self.avgTimeForOperationP[k][1]
                print "Operation: " + k + " Average response time: " + str(avgTime) + " in ms"
        print "****************************************************************************************************"

        return flag

    '''
    The Round Robin execution reads one line from each file at a time and creates a command object for that script line.
    '''
    def roundRobinExecution(self):
        nScriptFiles = len(self.scriptList)
        switch = False
        while True:
            #Read a line from each script
            for i in range(0, nScriptFiles):
                currentLineToExecute = self.scriptCurrentLine[i]
                if currentLineToExecute == len(self.scriptList[i]):
                    #We have fully executed this script
                    #print "Debug Info: The script number " + str(i + 1) + "has complete execution"
                    continue
                try:
                    commandString = self.scriptList[i][currentLineToExecute]
                    if len(commandString) < 1:
                        self.scriptCurrentLine[i] += 1
                        continue
                except:
                    print "Error: Problem with reading the script line"
                transactionId = self.transactionIdForScript[i]
                transactionMode = self.transactionModeForScript[i]
                commandObject = command(commandString, transactionId, transactionMode)
                flag = commandObject.processCommand()
                #print commandObject
                if flag == False:
                    return False

                #check if we have already executed this command
                if commandObject.commandtype == 'B':
                    startTime = time.time()
                    if self.committedTransaction.has_key((i, currentLineToExecute)) or self.committedProcess.has_key((i, currentLineToExecute)) or self.abortedProcess.has_key((i, currentLineToExecute)) or self.abortedTransaction.has_key((i, currentLineToExecute)):
                        while True:
                            self.scriptCurrentLine[i] += 1
                            if self.scriptCurrentLine[i] < len(self.scriptList[i]):
                                commandString = self.scriptList[i][currentLineToExecute]
                                transactionId = self.transactionIdForScript[i]
                                commandObject = command(commandString, transactionId, -1)
                                if commandObject.commandtype == 'B':
                                    break
                            else:
                                break


                #send this commandObject to the scheduler to execute the command
                #the transaction Manager is blocked while scheduler is working on this transaction
                #the transaction Manager will return an object of type result.
                operationStartTime = time.time()
                resultObject = self.SchedulerObject.execute(commandObject)
                operationEndTime = time.time()
                totalOperationTime = operationEndTime - operationStartTime
                if commandObject.commandMode == 0:
                    self.avgTimeForOperationP[commandObject.commandtype][0] += totalOperationTime
                    self.avgTimeForOperationP[commandObject.commandtype][1] += 1
                else:
                    self.avgTimeForOperationT[commandObject.commandtype][0] += totalOperationTime
                    self.avgTimeForOperationT[commandObject.commandtype][1] += 1


                #If command object is associated with a command BEGIN, the scheduler will return the transaction id of the transaction
                if resultObject.transactionId != -1:
                    self.transactionIdForScript[i] = resultObject.transactionId
                    self.transactionModeForScript[i] = commandObject.commandMode
                    if self.transactionScriptMap.has_key(resultObject.transactionId):
                        print "Error Message: Transaction Id should be unique. The transaction id: " + str(resultObject.transactionId) + " already exists"
                        return False
                    self.transactionScriptMap[resultObject.transactionId] = (i, currentLineToExecute)
                    self.transactionStartTime[resultObject.transactionId] = startTime
                    self.transactionStat[self.transactionScriptMap[resultObject.transactionId]] = [0,0,0,0] #no. of R, M, W, D in this transaction

                #Result object will have a boolean value specifying whether scheduler was able to execute the command or not
                if resultObject.successful == True:
                    self.scriptCurrentLine[i] += 1
                    currentTransactionId = commandObject.transactionId
                    #If the current command was a commit
                    if commandObject.commandtype == 'C':
                        #self.noOfRestarts = 0
                        endTime = time.time()
                        totalTime = endTime - self.transactionStartTime[currentTransactionId]
                        if commandObject.commandMode == 1:
                            self.committedTransaction[self.transactionScriptMap[currentTransactionId]] = totalTime
                        elif commandObject.commandMode == 0:
                            self.committedProcess[self.transactionScriptMap[currentTransactionId]] = totalTime
                    if commandObject.commandtype == 'A':
                        endTime = time.time()
                        totalTime = endTime - self.transactionStartTime[currentTransactionId]
                        if commandObject.commandMode == 1:
                            self.abortedTransaction[self.transactionScriptMap[currentTransactionId]] = totalTime
                        else:
                            self.abortedProcess[self.transactionScriptMap[currentTransactionId]] = totalTime
                    if commandObject.commandtype == 'R':
                        self.transactionStat[self.transactionScriptMap[currentTransactionId]][0] += 1
                    if commandObject.commandtype == 'M':
                        self.transactionStat[self.transactionScriptMap[currentTransactionId]][1] += 1
                    if commandObject.commandtype == 'W':
                        self.transactionStat[self.transactionScriptMap[currentTransactionId]][2] += 1
                    if commandObject.commandtype == 'D':
                        self.transactionStat[self.transactionScriptMap[currentTransactionId]][3] += 1

                #Result object can provide a signal to restart the any transaction
                if resultObject.restartId != -1:
                    (scriptNumberToRestart, lineInScriptToRestartFrom) = self.transactionScriptMap[resultObject.restartId]
                    self.scriptCurrentLine[scriptNumberToRestart] = len(self.scriptList[scriptNumberToRestart])
                    self.noOfRestarts += 1
                    print "HERE"
                    #Check if global condition of switching from OCC to 2PL have reached.
                    #We switch from OCC to 2PL when the number of restarts are above some threshold.
                    if self.noOfRestarts  > self.restartThreshold:
                        self.switch = True
                        break

            if self.switch == True:
                break
            #self.scriptCurrentLine[i] += 1
            # Find the number of scripts which are currently executing
            count = 0
            for i in range(0, nScriptFiles):
                if self.scriptCurrentLine[i] < len(self.scriptList[i]):
                    count += 1
            # If all the scripts have finsihed execution stop the loop
            if count == 0:
                return True
                break
        return False

    '''
    A Random execution chooses the script to execute in random order and executes a random number of lines from the selected
    script. 
    '''
    def randomExecution(self):
        random.seed(self.randomSeed)
        nScriptFiles = len(self.scriptList)

        while True:
            if self.switch == True:
                break
            #choose the script to be executed at random
            scriptNumber = random.randint(0,nScriptFiles-1)
            #choose the number of operations to be executed at random
            noOfOperationsLeftInScript = len(self.scriptList[scriptNumber]) - self.scriptCurrentLine[scriptNumber]
            if noOfOperationsLeftInScript == 0:
                # Find the number of scripts which are currently executing
                count = 0
                for i in range(0, nScriptFiles):
                    if self.scriptCurrentLine[i] < len(self.scriptList[i]):
                        count += 1
                # If all the scripts have finsihed execution stop the loop
                if count == 0:
                    return True
                    break
                else:
                    continue
            noOfOperations = random.randint(1,noOfOperationsLeftInScript)
            for i in range(0, noOfOperations):
                currentLineToExecute = self.scriptCurrentLine[scriptNumber]
                commandString = self.scriptList[scriptNumber][currentLineToExecute]
                if len(commandString) < 1:
                    self.scriptCurrentLine[scriptNumber] += 1
                    continue
                transactionId = self.transactionIdForScript[scriptNumber]
                transactionMode = self.transactionModeForScript[scriptNumber]
                commandObject = command(commandString, transactionId, transactionMode)
                flag = commandObject.processCommand()
                #print commandObject
                if flag == False:
                    return False

                # check if we have already executed this command
                if commandObject.commandtype == 'B':
                    startTime = time.time()
                    if self.committedTransaction.has_key((i, currentLineToExecute))or self.committedProcess.has_key((i, currentLineToExecute)) or self.abortedProcess.has_key((i, currentLineToExecute)) or self.abortedTransaction.has_key((i, currentLineToExecute)):
                        while True:
                            self.scriptCurrentLine[i] += 1
                            if self.scriptCurrentLine[i] < len(self.scriptList[i]):
                                commandString = self.scriptList[i][currentLineToExecute]
                                transactionId = self.transactionIdForScript[i]
                                commandObject = command(commandString, transactionId, -1)
                                if commandObject.commandtype == 'B':
                                    break
                            else:
                                break

                #send this commandObject to the scheduler to execute the command
                #the transaction Manager is blocked while scheduler is working on this transaction
                #the transaction Manager will return an object of type result.
                operationStartTime = time.time()
                resultObject = self.SchedulerObject.execute(commandObject)
                operationEndTime = time.time()
                totalOperationTime = operationEndTime - operationStartTime
                if commandObject.commandMode == 0:
                    self.avgTimeForOperationP[commandObject.commandtype][0] += totalOperationTime
                    self.avgTimeForOperationP[commandObject.commandtype][1] += 1
                else:
                    self.avgTimeForOperationT[commandObject.commandtype][0] += totalOperationTime
                    self.avgTimeForOperationT[commandObject.commandtype][1] += 1


                #If command object is associated with a command BEGIN, the scheduler will return the transaction id of the transaction
                if resultObject.transactionId != -1:
                    self.transactionIdForScript[scriptNumber] = resultObject.transactionId
                    self.transactionModeForScript[scriptNumber] = commandObject.commandMode
                    if self.transactionScriptMap.has_key(resultObject.transactionId):
                        print "Error Message: Transaction Id should be unique. The transaction id: " + str(resultObject.transactionId) + " already exists"
                        return False
                    self.transactionScriptMap[resultObject.transactionId] = (scriptNumber, currentLineToExecute)
                    self.transactionStartTime[resultObject.transactionId] = startTime
                    self.transactionStat[self.transactionScriptMap[resultObject.transactionId]] = [0, 0, 0, 0]  # no. of R, M, W, D in this transaction

                #Result object will have a boolean value specifying whether scheduler was able to execute the command or not
                if resultObject.successful == True:
                    self.scriptCurrentLine[scriptNumber] += 1
                    currentTransactionId = commandObject.transactionId
                    # If the current command was a commit
                    if commandObject.commandtype == 'C':
                        self.noOfRestarts = 0
                        endTime = time.time()
                        totalTime = endTime - self.transactionStartTime[currentTransactionId]
                        if commandObject.commandMode == 1:
                            self.committedTransaction[self.transactionScriptMap[currentTransactionId]] = totalTime
                        elif commandObject.commandMode == 0:
                            self.committedProcess[self.transactionScriptMap[currentTransactionId]] = totalTime
                    if commandObject.commandtype == 'A':
                        endTime = time.time()
                        totalTime = endTime - self.transactionStartTime[currentTransactionId]
                        if commandObject.commandMode == 1:
                            self.abortedTransaction[self.transactionScriptMap[currentTransactionId]] = totalTime
                        else:
                            self.abortedProcess[self.transactionScriptMap[currentTransactionId]] = totalTime
                    if commandObject.commandtype == 'R':
                        self.transactionStat[self.transactionScriptMap[currentTransactionId]][0] += 1
                    if commandObject.commandtype == 'M':
                        self.transactionStat[self.transactionScriptMap[currentTransactionId]][1] += 1
                    if commandObject.commandtype == 'W':
                        self.transactionStat[self.transactionScriptMap[currentTransactionId]][2] += 1
                    if commandObject.commandtype == 'D':
                        self.transactionStat[self.transactionScriptMap[currentTransactionId]][3] += 1

                #Result object can provide a signal to restart the any transaction
                if resultObject.restartId != -1:
                    (scriptNumberToRestart, lineInScriptToRestartFrom) = self.transactionScriptMap[resultObject.restartId]
                    self.scriptCurrentLine[scriptNumberToRestart] = lineInScriptToRestartFrom
                    self.noOfRestarts += 1
                    # Check if global condition of switching from OCC to 2PL have reached.
                    # We switch from OCC to 2PL when the number of restarts are above some threshold.
                    if self.noOfRestarts > self.restartThreshold:
                        self.switch = True
                        break


                #self.scriptCurrentLine[scriptNumber] += 1

    '''
    The below function switch the scheduler from OCC (default) to 2PL
    '''
    def switchTo2PL(self):
        #Flush all the past data
        self.scriptCurrentLine = [0 for i in range(0, len(self.scriptCurrentLine))]
        self.transactionIdForScript = [-1 for i in range(0, len(self.scriptCurrentLine))]
        self.transactionModeForScript = [-1 for i in range(0, len(self.scriptCurrentLine))]
        self.noOfRestarts  = 0
        self.SchedulerObject = self.SchedulerObject2PL
        self.transactionScriptMap = {}
        self.transactionStartTime = {}
        self.switch = False
        self.execute()









class command(object):
    commandMode = -1 #Mode of the command. commandMode = 1 for Trnsaction or commandMode = 0 for Process
    commandtype = "" #The type of the command e.g. Begin, Commit, Abort, Read, MultipleRead, Write, Delete
    recordId = -1
    tableName = ""
    transactionId = -1
    recordClientName = ""
    recordPhone = ""

    def __init__(self, commandString, transactionID, mode):
        commandString = commandString.replace(', ',',')
        commandString = commandString.replace(',',', ')
        self.commandString = commandString
        self.transactionId = transactionID
        self.commandMode = mode

    def processCommand(self):
        cs = self.commandString.split(' ')
        #Check the starting alphabet of the command string. It specifies the type of command
        if cs[0].strip() == 'B':
            self.commandtype = 'B'
            if cs[1].strip() == '1':
                self.commandMode = 1
            else:
                self.commandMode = 0
            return True

        #Sanity check
        #if its any other type of operation then there must be a transaction id associated with it.
        if self.transactionId == -1:
            print "Error Message: There should be a transaction Id associated with this operation: " + self.commandString
            #return False

        if cs[0].strip() == 'R' or cs[0].strip() == 'M':
            try:
                self.commandtype = cs[0].strip()
                self.tableName = cs[1].strip()
                self.recordId = int(cs[2].strip())
            except:
                print "Error: The script is not in correct format. Invalid line: " + self.commandString + " in trasaction id: " + str(self.transactionId)
                return False
        if cs[0].strip() == 'W':
            try:
                self.commandtype = 'W'
                self.tableName = cs[1].strip()
                cs[2] = cs[2].strip()
                self.recordId = int(cs[2][1:len(cs[2])-1])
                cs[3] = cs[3].strip()
                self.recordClientName = cs[3][0:len(cs[3])-1]
                cs[4] = cs[4].strip()
                self.recordPhone = cs[4][0:len(cs[4])-1]
            except:
                print "Error: The script is not in correct format. Invalid line: " + self.commandString + " in trasaction id: " + str(self.transactionId)
                return False
        if cs[0].strip() == 'C' or cs[0].strip() == 'A':
            try:
                self.commandtype = cs[0].strip()
            except:
                print "Error: The script is not in correct format. Invalid line: " + self.commandString + " in trasaction id: " + str(self.transactionId)
                return False
        if cs[0].strip() == 'D':
            try:
                self.commandtype = 'D'
                self.tableName = cs[1].strip()
            except:
                print "Error: The script is not in correct format. Invalid line: " + self.commandString + " in trasaction id: " + int(self.transactionId)
                return False

        return True

    def __str__(self):
        string = "Command Type: " + self.commandtype + "\n Command Mode: " + str(self.commandMode) + "\n Record Id: " +  str(self.recordId)  + "\n Table Name: " + str(self.tableName) + "\n Transaction Id: " + str(self.transactionId)
        return string

